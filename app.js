const fs = require('fs');
const getUrls = require('get-urls');
const ProgressBar = require('progress');
const Dedupe = require('array-dedupe');

let fileUrls = [];

var textUrls = fs.readFileSync("./.knownUrls")
let knownUrls = textUrls.toString().split('\n')

var path = process.argv[2];

function getFiles (dir, files_){
    files_ = files_ || [];
    var files = fs.readdirSync(dir);
    for (var i in files){
        var name = dir + '/' + files[i];
        if (fs.statSync(name).isDirectory()){
            getFiles(name, files_);
        } else {
            files_.push(name);
        }
    }
    return files_;
}

let files = getFiles(path);

var bar = new ProgressBar(' scanning files [:bar] :percent ', { total: files.length });

files.forEach(element => {
    bar.tick();
    var contents = fs.readFileSync(element, 'utf-8');
    var urls = getUrls(contents)
    urls.forEach(function(val1, val2, set) {
        fileUrls.push(val1)
    });
});

console.log("Scan complete.  Dedupe and remove known addresses");

Dedupe(fileUrls)

let final = fileUrls;

knownUrls.forEach(knownVal => {
    final = final.filter((url) => { return !(url.indexOf(knownVal) > 0) })
})

console.log("done")

final.sort();
final.forEach(link => {
    console.log(link)
});

console.log("")
console.log(`Found ${fileUrls.length} URLs in ${files.length} files.  After filtering, found ${final.length} URLs to check`)
