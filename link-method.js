const blc = require("broken-link-checker")

let pageUrl = "http://localhost:4567"

let ignoreDomains = [
	'localhost:4567',
	'docs.gitlab.com',
	'gitlab.com'
]

var htmlUrlChecker = new blc.HtmlUrlChecker({}, {
	html: function(tree, robots, response, pageUrl, customData){},
	junk: function(result, customData){},
	link: function(result, customData){
		//console.log(result.url);
		if (!(new RegExp(ignoreDomains.join("|")).test(result.url.resolved))) {
			console.log(result.url.original);
		}
		
	},
	end: function(){}
});

htmlUrlChecker.enqueue(pageUrl, {});
